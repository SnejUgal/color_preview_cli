use css_color::Rgba;
use palette::{Srgb, Srgba};
use termcolor::Color;

pub fn is_bright(color: Srgb<u8>) -> bool {
    let red = color.red as f64 / 255.0;
    let green = color.green as f64 / 255.0;
    let blue = color.blue as f64 / 255.0;

    let brightness = 0.2126 * red + 0.7152 * green + 0.0722 * blue;

    brightness > 0.6
}

pub fn overlay((color, alpha): (Srgb<u8>, f64), background: Srgb<u8>) -> Srgb<u8> {
    let color_red = color.red as f64;
    let color_green = color.green as f64;
    let color_blue = color.blue as f64;

    let background_red = background.red as f64;
    let background_green = background.green as f64;
    let background_blue = background.blue as f64;

    let red = alpha * (color_red - background_red) + background_red;
    let green = alpha * (color_green - background_green) + background_green;
    let blue = alpha * (color_blue - background_blue) + background_blue;

    Srgb::new(red.round() as u8, green.round() as u8, blue.round() as u8)
}

pub fn to_termcolor(color: Srgb<u8>) -> Color {
    Color::Rgb(color.red, color.green, color.blue)
}

pub fn parse(string: &str) -> Option<Srgba<u8>> {
    let Rgba {
        red,
        green,
        blue,
        alpha,
    } = string.parse().ok()?;

    Some(Srgba::new(
        (red * 255.0) as u8,
        (green * 255.0) as u8,
        (blue * 255.0) as u8,
        (alpha * 255.0) as u8,
    ))
}

pub fn without_alpha(color: Srgba<u8>) -> Srgb<u8> {
    Srgb::new(color.red, color.green, color.blue)
}
