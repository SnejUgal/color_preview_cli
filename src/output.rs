use std::io::prelude::*;
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};
use terminal_size::{terminal_size, Height, Width};

const LABEL_PADDING: usize = 5;

pub struct Output {
    output: StandardStream,

    width: usize,
    height: usize,

    primary: ColorSpec,
    secondary: ColorSpec,
    padding: ColorSpec,
}

impl Output {
    pub fn new(primary: Color, secondary: Color, background: Color) -> Self {
        let mut primary_spec = ColorSpec::new();
        primary_spec.set_fg(Some(primary));
        primary_spec.set_bold(true);
        primary_spec.set_bg(Some(background));

        let mut secondary_spec = ColorSpec::new();
        secondary_spec.set_fg(Some(secondary));
        secondary_spec.set_bold(true);
        secondary_spec.set_bg(Some(background));

        let (Width(width), Height(height)) = match terminal_size() {
            Some(size) => size,
            None => {
                eprintln!("error: unable to get terminal size");
                std::process::exit(1);
            }
        };

        Self {
            output: StandardStream::stdout(ColorChoice::Always),

            width: width as usize,
            height: height as usize,

            primary: primary_spec,
            secondary: secondary_spec,
            padding: ColorSpec::new(),
        }
    }

    pub fn empty_line(&mut self) {
        writeln!(&mut self.output).unwrap();
    }

    pub fn padding_line(&mut self) {
        write!(&mut self.output, "  ").unwrap();
        self.output.set_color(&self.primary).unwrap();

        for _ in 2..self.width - 2 {
            write!(&mut self.output, " ").unwrap();
        }

        self.output.set_color(&self.padding).unwrap();
        writeln!(&mut self.output).unwrap();
    }

    pub fn entry(&mut self, label: &str, value: &str) {
        write!(&mut self.output, "  ").unwrap();
        self.output.set_color(&self.secondary).unwrap();
        write!(
            &mut self.output,
            "  {label:>padding$}:",
            label = label,
            padding = LABEL_PADDING
        )
        .unwrap();
        self.output.set_color(&self.primary).unwrap();
        write!(&mut self.output, " {}", value).unwrap();

        for _ in 2 + 2 + LABEL_PADDING + 2 + value.len()..self.width - 2 {
            write!(&mut self.output, " ").unwrap();
        }

        self.output.set_color(&self.padding).unwrap();
        writeln!(&mut self.output).unwrap();
    }
}
