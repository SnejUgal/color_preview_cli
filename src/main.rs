use palette::Srgb;

mod color;
mod output;

use output::Output;

fn main() {
    let mut string_color: String = String::new();

    std::env::args().skip(1).for_each(|arg| {
        string_color.extend(arg.chars());
        string_color.push(' ');
    });

    let color = match color::parse(string_color.trim()) {
        Some(color) => color,
        None => {
            eprintln!("error: could not parse color");
            std::process::exit(1);
        }
    };
    let without_alpha = color::without_alpha(color);

    let text = if color::is_bright(without_alpha) {
        Srgb::new(0, 0, 0)
    } else {
        Srgb::new(255, 255, 255)
    };

    let primary = color::to_termcolor(color::overlay((text, 0.9), without_alpha));
    let secondary = color::to_termcolor(color::overlay((text, 0.5), without_alpha));

    let background = color::to_termcolor(without_alpha);

    let mut output = Output::new(primary, secondary, background);

    output.empty_line();
    output.padding_line();

    let hex = format!(
        "#{:0>2x}{:0>2x}{:0>2x}{:0>2x}",
        color.red, color.green, color.blue, color.alpha,
    );
    output.entry("#rgba", &hex);

    output.padding_line();

    output.entry("red", &color.red.to_string());
    output.entry("green", &color.green.to_string());
    output.entry("blue", &color.blue.to_string());
    output.entry("alpha", &color.alpha.to_string());

    output.padding_line();
    output.empty_line();
}
